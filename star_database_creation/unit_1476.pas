
unit unit_1476;

{$MODE Delphi}

interface
var
  tof    : file;
  nr_of_stars           : longint;
  nr_of_skipped         : longint;


type
hnskyhdr1476_A7 = packed record {interim record}
             ra7 : byte;
             ra8 : byte;
             ra9 : byte;
             dec7: byte;
             dec8: byte;    {note with word and smallint, delphi makes longer records !!!}
             dec9: shortint;
             Bp_Rp: shortint;{blue minus red magnitude}
   end;

hnskyhdr1476_A6 = packed record {interim record}
             ra7 : byte;
             ra8 : byte;
             ra9 : byte;
             dec7: byte;
             dec8: byte;    {note with word and smallint, delphi makes longer records !!!}
             dec9: shortint;
   end;
hnskyhdr1476_5 = packed record
             ra7 : byte;
             ra8 : byte;
             ra9 : byte;
             dec7: byte;
             dec8: byte;    {note with word and smallint, delphi makes longer records !!!}
   end;

hnskyhdr1476_6 = packed record
             ra7 : byte;
             ra8 : byte;
             ra9 : byte;
             dec7: byte;
             dec8: byte;    {note with word and smallint, delphi makes longer records !!!}
             Bp_Rp: shortint;{blue minus red magnitude}
   end;



var
   Numwritten                  : integer;
   new_magnitude_record_written : array[1..1801] of integer;{keep record if new magnitude step record is written to file for 6 byte records}

function limit_radialen(z:double):double;

function name1476(ra,dec:real): string; {give segment name}


function filenames1476(nr:integer): string;


implementation
uses  sysutils;

Const

Stepsize=90/17.5;{5.142857143 degrees}

rings1476: array[0..36] of real=
// (-90,-85.23224404, -75.66348756, -65.99286637, -56.14497387,-46.03163067,-35.54307745,-24.53348115,-12.79440589, 0.0 ,+12.79440589,+24.53348115,+35.54307745,+46.03163067,+56.14497387,+65.99286637,+75.66348756,+85.23224404,90);
 (-90,	{	90	}
 -stepsize*17,	{	87.42857143	}
 -stepsize*16,	{	82.28571429	}
 -stepsize*15,	{	77.14285714	}
 -stepsize*14,	{	72	}
 -stepsize*13,	{	66.85714286	}
 -stepsize*12,	{	61.71428571	}
 -stepsize*11,	{	56.57142857	}
 -stepsize*10,	{	51.42857143	}
 -stepsize*9,	{	46.28571429	}
 -stepsize*8,	{	41.14285714	}
 -stepsize*7,	{	36	}
 -stepsize*6,	{	30.85714286	}
 -stepsize*5,	{	25.71428571	}
 -stepsize*4,	{	20.57142857	}
 -stepsize*3,	{	15.42857143	}
 -stepsize*2,	{	10.28571429	}
 -stepsize*1,	{	5.142857143	}
  stepsize*0,	{	0	}
  stepsize*1,	{	-5.142857143	}
  stepsize*2,	{	-10.28571429	}
  stepsize*3,	{	-15.42857143	}
  stepsize*4,	{	-20.57142857	}
  stepsize*5,	{	-25.71428571	}
  stepsize*6,	{	-30.85714286	}
  stepsize*7,	{	-36	}
  stepsize*8,	{	-41.14285714	}
  stepsize*9,	{	-46.28571429	}
  stepsize*10,	{	-51.42857143	}
  stepsize*11,	{	-56.57142857	}
  stepsize*12,	{	-61.71428571	}
  stepsize*13,	{	-66.85714286	}
  stepsize*14,	{	-72	}
  stepsize*15,	{	-77.14285714	}
  stepsize*16,	{	-82.28571429	}
  stepsize*17,	{	-87.42857143	}
  90); 	{	-90	}



seg_length=36;

segments1476: array[1..seg_length] of integer= {number of division in each latitude ring}
 {RA cells		RA step distance north[degr]		RA step distance south[degr] }
 (1	,
 3	,{	5.383779642	,	16.1079919	}
 9	,{	5.369330633	,	8.900837358	}
 15	,{	5.340502415	,	7.416407865	}
 21	,{	5.297434189	,	6.737571971	}
 27	,{	5.240333755	,	6.318248833	}
 33	,{	5.169476318	,	6.009785252	}
 38	,{	5.219024035	,	5.906745491	}
 43	,{	5.21991462	,	5.785640782	}
 48	,{	5.182969867	,	5.648035995	}
 52	,{	5.213571688	,	5.600886884	}
 56	,{	5.200823535	,	5.518599387	}
 60	,{	5.150692762	,	5.405813207	}
 63	,{	5.148393531	,	5.349913547	}
 65	,{	5.185300822	,	5.338871228	}
 67	,{	5.179501938	,	5.286785849	}
 68	,{	5.209038998	,	5.272805086	}
 69	,{	5.196387621	,	5.217391304	}

 69	,{	5.217391304	,	5.196387621	}
 68	,{	5.272805086	,	5.209038998	}
 67	,{	5.286785849	,	5.179501938	}
 65	,{	5.338871228	,	5.185300822	}
 63	,{	5.349913547	,	5.148393531	}
 60	,{	5.405813207	,	5.150692762	}
 56	,{	5.518599387	,	5.200823535	}
 52	,{	5.600886884	,	5.213571688	}
 48	,{	5.648035995	,	5.182969867	}
 43	,{	5.785640782	,	5.21991462	}
 38	,{	5.906745491	,	5.219024035	}
 33	,{	6.009785252	,	5.169476318	}
 27	,{	6.318248833	,	5.240333755	}
 21	,{	6.737571971	,	5.297434189	}
 15	,{	7.416407865	,	5.340502415	}
 9	,{	8.900837358	,	5.369330633	}
 3	,{	16.1079919	,	5.383779642	}
 1);



function filenames1476(nr:integer): string;
var
  la,lo,m,o : integer;
  lat,seg : string;
begin
  //const
  //filenames1476 : array[1..1476] of string= {}
  //(('0101.1476'),

  // ('0201.1476'),
  // ('0202.1476'),
  // ('0203.1476'),

  // ('0301.1476'),
  // ('0302.1476'),
  m:=0;
  la:=1;
  repeat
   lo:=segments1476[la];
   m:=m+lo;
   if nr<=m then
   begin
     str(la,lat); if length(lat)=1 then lat:='0' +lat;
     str(nr-(m-lo),seg); if length(seg)=1 then seg:='0' +seg;
     result:=lat+seg+'.1476';
     exit;
   end;
   inc(la);
  until la>seg_length; {36}
end;

function name1476(ra,dec:real): string; {give segment name}
var
   r,d : integer;
   found: boolean;
   dum  :string;
begin
  found:=false;
  d:=0;

  if dec>+pi/2 then dec:=PI/2;
  if dec<-pi/2 then dec:=-PI/2;
  repeat {find dec ring}
    inc(d);
    if dec*180/pi<rings1476[d] then begin found:=true; str(d,result); if length(result)=1 then result:='0' +result; end;

  until ((found=true) or (d>=seg_length));

 {find ra segment}
  if ra>=2*pi then ra:=0;
  if ra<0     then ra:=0;
  str( 1+trunc(segments1476[d]*ra/(2*pi)),dum);
  if length(dum)=1 then dum:='0' +dum;
  result:=result+dum+'.1476';
end;

PROCEDURE ang_sep(ra1,dec1,ra2,dec2 : real;var sep: real); { By Han Kleijn}
var cos_sep:real;
{caculates angular separation. according formula 9.1 old meeus}
Begin
  cos_sep:=sin(dec1)*sin(dec2)+ cos(dec1)*cos(dec2)*cos(ra1-ra2);
  if ABS(cos_sep)<1 then begin sep:=(PI/2-ARCTAN(cos_sep/(SQRT(1-SQR(cos_sep))))) end {arccos function}
  else
  begin if cos_sep>0 then sep:=0 {note bij x=1 klapt uitkomst net om}
                     else sep:=PI;
  end; {arccos function}

end;
function  limit_radialen(z:double):double;
begin
  while z<0 do z:=z+2*pi;
  while z>=2*pi do z:=z-2*pi; {corrected 2019}
  limit_radialen:=z;
end;


begin

end.
